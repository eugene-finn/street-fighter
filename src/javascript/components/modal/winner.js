import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  showModal({
    title: `The winner is ${fighter.name} 🎉`,
    bodyElement: 'Try again!',
    onClose: () => {
      window.location.reload();
    },
  });
}
