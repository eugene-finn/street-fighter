import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    const image = createFighterImage(fighter);
    fighterElement.appendChild(image);
    const { _id, name, health, attack, defense } = fighter;
    const info = createFighterInfo(`
    <div>${name}</div>
    <div> ❤️ ${health} 👊 ${attack} 🛡️ ${defense}</div>
    `);

    fighterElement.appendChild(info);
  }

  return fighterElement;
}

export function createFighterInfo(fighterInfoString) {
  const info = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  });
  info.innerHTML = fighterInfoString;
  return info;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
