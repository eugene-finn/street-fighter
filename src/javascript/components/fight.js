import { controls } from '../../constants/controls';

const {
  PlayerOneAttack,
  PlayerOneBlock,
  PlayerTwoBlock,
  PlayerTwoAttack,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination,
} = controls;

export const setIndicatorWidth = ({ id, health, initialHealth }) => {
  const healthWidth = (health / initialHealth) * 100;
  const healthTag = document.getElementById(id);

  healthTag.style = `width: ${healthWidth}%`;
};

export const hitPlayerCombinationChecker = (collectionSet, hitPlayerCombination) => {
  return (
    hitPlayerCombination.length === collectionSet.size &&
    hitPlayerCombination.every((element) => collectionSet.has(element))
  );
};

export async function fight(firstFighter, secondFighter) {
  const health1 = firstFighter.health;
  const health2 = secondFighter.health;
  const keyCombination = new Set();

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keypress', function (event) {
      event.stopImmediatePropagation();
      keyCombination.add(event.code);
    });

    document.addEventListener('keyup', (event) => {
      event.stopImmediatePropagation();
      if (firstFighter.health === 0) {
        return resolve(secondFighter);
      }
      if (secondFighter.health === 0) {
        return resolve(firstFighter);
      }
      const { code } = event;

      if (code === PlayerOneBlock && firstFighter.isBlocking) {
        console.log('первый убрал блок');
        firstFighter.isBlocking = false; // D block
      }

      if (code === PlayerTwoBlock && secondFighter.isBlocking) {
        console.log('второй убрал блок');
        secondFighter.isBlocking = false; // D block
      }
      const isPlayerOnePressedHitCombination = hitPlayerCombinationChecker(
        keyCombination,
        PlayerOneCriticalHitCombination
      );
      const isPlayerTwoPressedHitCombination = hitPlayerCombinationChecker(
        keyCombination,
        PlayerTwoCriticalHitCombination
      );

      if (!isPlayerOnePressedHitCombination && !isPlayerTwoPressedHitCombination) {
        keyCombination.clear();
      }
    });

    document.addEventListener('keydown', (event) => {
      event.stopImmediatePropagation();
      const { code } = event;

      if (code === PlayerTwoBlock && !secondFighter.isBlocking) {
        secondFighter.isBlocking = true; // D block
        console.log('PlayerTwo выставил блок');
      }

      if (code === PlayerOneBlock && !firstFighter.isBlocking) {
        firstFighter.isBlocking = true; // D block
        console.log('PlayerOne выставил блок', firstFighter);
      }

      if (code === PlayerOneAttack) {
        const damage = getDamage(firstFighter, secondFighter); // A
        const health = Math.max(0, secondFighter.health - damage);
        setIndicatorWidth({ id: 'right-fighter-indicator', health, initialHealth: health2 });

        secondFighter.health = health;
        console.log('PlayerOne атакует KeyA', health);
        return;
      }

      if (code === PlayerTwoAttack) {
        const damage = getDamage(secondFighter, firstFighter); // J
        const health = Math.max(0, firstFighter.health - damage);

        setIndicatorWidth({ id: 'left-fighter-indicator', health, initialHealth: health1 });

        firstFighter.health = health;
        console.log('PlayerTwo атакует KeyJ', health);
        return;
      }
      const isPlayerOnePressedHitCombination = hitPlayerCombinationChecker(
        keyCombination,
        PlayerOneCriticalHitCombination
      );
      const isPlayerTwoPressedHitCombination = hitPlayerCombinationChecker(
        keyCombination,
        PlayerTwoCriticalHitCombination
      );
      const isPlayerOnePressedHitMoreThan10SecAgo =
        !firstFighter.pressedHitCombinationLastTime || new Date() - firstFighter.pressedHitCombinationLastTime > 10000;

      const isPlayerTwoPressedHitMoreThan10SecAgo =
        !secondFighter.pressedHitCombinationLastTime ||
        new Date() - secondFighter.pressedHitCombinationLastTime > 10000;

      if (isPlayerOnePressedHitCombination && isPlayerOnePressedHitMoreThan10SecAgo) {
        firstFighter.pressedHitCombinationLastTime = new Date();
        const criticalHitDamage = getСriticalHitCombination(firstFighter);
        const health = Math.max(0, secondFighter.health - criticalHitDamage);
        setIndicatorWidth({ id: 'right-fighter-indicator', health, initialHealth: health2 });

        console.log('PlayerOne атакует hit combination');
        secondFighter.health = health;

        return keyCombination.clear();
      }

      if (isPlayerTwoPressedHitCombination && isPlayerTwoPressedHitMoreThan10SecAgo) {
        secondFighter.pressedHitCombinationLastTime = new Date();
        const criticalHitDamage = getСriticalHitCombination(secondFighter);
        const health = Math.max(0, firstFighter.health - criticalHitDamage);
        setIndicatorWidth({ id: 'left-fighter-indicator', health, initialHealth: health1 });
        console.log('PlayerTwo атакует hit combination');

        firstFighter.health = health;
        return keyCombination.clear();
      }
    });
  });
}

function randomChance(number) {
  // random from min to max
  return Math.random() + number;
}

export function getDamage(attacker, defender) {
  //атакующий зажал одновременно блок
  if (attacker.isBlocking) {
    console.log('атакующий зажал одновременно блок');
    return 0;
  }
  let damage = 0;
  if (defender.isBlocking) {
    damage = getHitPower(attacker) - getBlockPower(defender);
    return damage > 0 ? damage : 0;
  }
  damage = getHitPower(attacker);
  console.log('защищающийся пропустил удар');
  //защищающийся пропустил удар
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = randomChance(1);

  return fighter.attack * criticalHitChance;
}
export function getBlockPower(fighter) {
  const dodgeChance = randomChance(1);

  return fighter.defense * dodgeChance;
}

export function getСriticalHitCombination(fighter) {
  return fighter.attack * 2;
}
