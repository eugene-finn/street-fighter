export const controls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO'],
};

// Бойцы также могут наносить критические удары,
// которые не могут быть заблокированы и вычисляются по формуле
// 2 * attack, где attack - характеристика бойца.Для того,
// чтобы бойцу нанести такой удар,
// нужно одновременно нажать 3 соответствующие клавиши,
// указанные в файле controls.js.
// Этот удар можно наносить не чаще, чем каждые 10 секунд.
